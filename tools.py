import copy

def mean(x):
    return sum(x)/len(x)

#Equation 1
def sample_std_dev(x):
    mu = mean(x)
    variance = 0
    N = len(x)
    for xi in x:
        variance += pow(abs(xi - mu), 2)
    variance /= (N-1)
    std_dev = pow(variance, .5)
    return std_dev

#Equation 2
def r_coeff(x, y):
    numerator = 0
    denominator = 0
    N=len(x)
    for i in range(N):
        numerator += x[i]*y[i]
    numerator -= sum(x)*sum(y)/N
    
    sum_x_squared = sum(map(lambda xi:xi*xi,x))
    sum_y_squared = sum(map(lambda yi:yi*yi,y))
    x_half_denominator = pow( (sum_x_squared - pow(sum(x),2)/N) , .5)
    y_half_denominator = pow( (sum_y_squared - pow(sum(y),2)/N) , .5)
    denominator = x_half_denominator*y_half_denominator

    r = numerator/denominator
    return r

def r_squared(x, y):
	return pow(r_coeff(x,y),2)

#Equation 3
def slope_linreg(x, y):
    return r_coeff(x,y)*sample_std_dev(y)/sample_std_dev(x)

#Equation 4
def intercept_linreg(x, y):
    return mean(y) - slope_linreg(x, y)*mean(x)

#Equation 5
def print_coefficients(x,y):
    print(f"y = ax + b\na: {intercept_linreg(x,y)}\nb: {slope_linreg(x,y)}")

#Equation 6
def two_std_devs(x, y):
	#y = a + bx
	b = slope_linreg(x, y)
	a = intercept_linreg(x, y)
	SSE = 0
	N = len(x)
	for i in range(N):
		yi = y[i]
		y_hat = a + b * x[i]
		SSE += pow( abs(yi - y_hat), 2 )
	std_dev = pow( (SSE/N-2) , .5) 
	return 2*std_dev

#use x_no_outliers,y_no_outliers = map(list, zip(*remove_outliers(x,y))) 
def remove_outliers(x,y):
	finalx = []
	finaly = []
	b = slope_linreg(x, y)
	a = intercept_linreg(x, y)
	max_diff = two_std_devs(x, y)
	N = len(x)
	for i in range(N):
		yi = y[i]
		y_hat = a + b * x[i]
		if abs(yi - y_hat) > max_diff:
			continue
		else:
			finalx += [x[i]]
			finaly += [y[i]]
	return zip(finalx, finaly)

def population():
	#X = State Population
	x =  [4863300,741894,6931071,2988248,39250017,5540545,3576452,952065,681170,20612439,10310371,1428557,1683140,12801539,6633053,3134693,2907289,4436974,4681666,1331479,6016447,6811779,9928300,5519952,2988726,6093000,1042520,1907116,2940058,1334795,8944469,2081015,19745289,10146788,757952,11614373,3923561,4093465,12784227,1056426,4961119,865454,6651194,27862596,3051217,624594,8411808,7288000,1831102,5778708,585501]
	#Y =  Fatal Car Crashes
	y =  [937,78,865,488,3357,558,281,116,26,2933,1422,109,232,1003,768,356,381,763,704,151,472,359,980,357,628,868,171,194,303,130,569,358,965,1348,102,1053,624,446,1088,48,936,103,966,3407,259,57,722,504,250,544,100]
	assert len(x) == len(y)
	original_count = len(x)
	
	#Remove original residuals > 2SD
	x,y = map(list, zip(*remove_outliers(x,y)))
	
	#Restrict x domain
	x_dr_restricted = []
	y_dr_restricted  = []
	mux = mean(x)
	stddevx = sample_std_dev(x)
	muy = mean(y)
	stddevy = sample_std_dev(y)
	for i in range(len(x)):
		if abs(x[i] - mux) <= 2*stddevx and abs(y[i] - muy) <= 2*stddevy:
			x_dr_restricted  += [x[i]]
			y_dr_restricted  += [y[i]]
	
	#remove outliers
	x, y = x_dr_restricted, y_dr_restricted	
	
	print(f"Removed {original_count - len(x)} outliers")
	print(f"r: {r_coeff(x,y)}")
	print(f"r^2: {r_squared(x,y)}")
	print_coefficients(x,y)
	'''
	print(f"State Pop. Fatal Car Crashes")
	for i in range(len(x)):
		print(f"{x[i]} {y[i]}")
	'''

def age():
	#X = Age
	x = [13,14,15,16,17,18,19,22,27,45,65]#,85
	#Y = Deaths
	y = [41.09703,57.312234,121.712739,277.23956,437.320104,594.921723,598.33068,3133.34392,2495.106356,9145.677744,2468.612092]#,3534.228208
	assert len(x) == len(y)
	original_count = len(x)
	
	#Remove original residuals > 2SD
	x,y = map(list, zip(*remove_outliers(x,y)))
	
	print(f"Removed {original_count - len(x)} outliers")
	print(f"r: {r_coeff(x,y)}")
	print(f"r^2: {r_squared(x,y)}")
	print_coefficients(x,y)
	print(f"Age Deaths")
	for i in range(len(x)):
		print(f"{x[i]} {y[i]}")

def alcohol():
	#X is time mapped, aka (military time + 1800)/300 mod 8
	x = [0,1,2,3,4,5,6,7]
	#Y is percentage of drivers with BACs > 0.08
	y = [14,9,10,18,32,45,60,45]
	assert len(x) == len(y)
	original_count = len(x)
	
	#Remove original residuals > 2SD
	x,y = map(list, zip(*remove_outliers(x,y)))
	
	print(f"Removed {original_count - len(x)} outliers")
	print(f"r: {r_coeff(x,y)}")
	print(f"r^2: {r_squared(x,y)}")
	print_coefficients(x,y)
	print(f"Alcohol Plot")
	for i in range(len(x)):
		print(f"{x[i]} {y[i]}")

def main():
    population()
    age()
    alcohol()

if __name__ == '__main__':
    main()
	